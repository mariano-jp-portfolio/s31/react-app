// Base import
import React from 'react';

// Components
// default export
import FC from './components/Function';
import Classprops from './Classprops';
// named export
import {Class, ClassComp} from './components/Class';

export default class App extends React.Component {
  render(){
      return (
        <div className="App">
        <h1>Hello, earth!</h1>
        <h2>Hi, moon!</h2>
        <Classprops name="Pao" age="27" />
        <Classprops name="Janine" age="18" />
        <Classprops name="Jacob" age="24" />
        <FC />
        <Class />
        <ClassComp />
        </div>
    );
  };
}