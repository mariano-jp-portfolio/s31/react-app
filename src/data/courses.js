// Course Model
const coursesData = [
	{
		id: "wdc-001",
		name: "PHP-Laravel",
		description: "The most popular programming language",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc-002",
		name: "Python-Django",
		description: "Multi-purpose programming language",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc-003",
		name: "Java-Springboot",
		description: "One of the popular server-side programming language",
		price: 55000,
		onOffer: true
	}
];

export default coursesData;