// Base import
import React, { Fragment } from 'react';

// React-bootstrap
import { Container } from 'react-bootstrap';

// Import component
import NavBar from './components/Navbar';

// Import Page
// import Home from './pages/Home';
// import Courses from './pages/Courses';
import Login from './pages/Login';
// import Register from './pages/Register';

// Export
export default function App() {	
	return (
		<Fragment>
			<NavBar />
			<Container className="my-5">
				<Login />
			</Container>
		</Fragment>
	);
}