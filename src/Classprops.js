// Base import
import React, {Component} from 'react';

export default class Classprops extends Component {
	render() {
		return (
			<div>
				<h3>My name is {this.props.name}, I'm {this.props.age} years old</h3>
			</div>
		);
	}
}