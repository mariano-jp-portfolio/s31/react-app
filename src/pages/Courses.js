// COURSES PAGE
// Base import
import React, { Fragment } from 'react';

// Courses Data import
import coursesData from '../data/courses';

// Component import
import Course from '../components/Course';

// Export
export default function Courses() {
	// This will map our database
	const CourseCards = coursesData.map((course) => {
		// iterates over each element of array and returns a new array
		// Keys - help React identify which items have changed, are added, or are removed. The best way to pick a key is to use a string that uniquely identifies a list item among its siblings
		// Key-value pairs (objects)
		return <Course key={course.id} course={course} />
	});
	// returns course prop that is to be used by Course.js component
	return (
		<Fragment>{CourseCards}</Fragment>
	);
}