// Base import
import React, { useState, useEffect } from 'react';

// React-bootstrap
import { Form, Container, Button } from 'react-bootstrap';

// Export Register form
export default function Register() {
	// States
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [passwordConfirm, setPasswordConfirm] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);
	
	// To handle changes to our form inputs
	const handleEmail = (event) => {
		setEmail(event.target.value);
	};
	
	const handlePassword = (event) => {
		setPassword(event.target.value);
	};
	
	const handlePasswordConfirm = (event) => {
		setPasswordConfirm(event.target.value);
	};
	
	const register = (event) => {
		// prevents page redirection
		event.preventDefault();
		
		// clears the input fields
		setEmail("");
		setPassword("");
		setPasswordConfirm("");
		
		// successful registration
		alert("Registration successful. You may now log in");
	}
	
	// To verify user's input
	useEffect(() => {
		let isEmailNotEmpty = email !== "";
		let isPasswordNotEmpty = password !== "";
		let isPasswordConfirmNotEmpty = passwordConfirm !== "";
		let isPasswordMatched = password === passwordConfirm;
		
		// Determine if all conditions have been met
		if (isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatched) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password, passwordConfirm]);
	
	return (
		<Container>
			<h3>Register</h3>
			<Form id="register" onSubmit={(e) => register(e)}>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Enter email" 
						value={email} 
						onChange={handleEmail} 
						required
					/>
					<Form.Text className="text-muted">
						We will never share your information with anyone else.
					</Form.Text>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Password" 
						value={password} 
						onChange={handlePassword} 
						required
					/>
				</Form.Group>
				
				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Verify your password" 
						value={passwordConfirm} 
						onChange={handlePasswordConfirm} 
						required 
					/>
				</Form.Group>
				<Button 
					variant="primary" 
					type="submit" 
					disabled={isDisabled}
				>
					Submit
				</Button>
			</Form>
		</Container>
	);
}