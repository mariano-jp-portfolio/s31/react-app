// Base import
import React, { useState, useEffect } from 'react';

// React-bootstrap
import { Container, Form, Button } from 'react-bootstrap';

// Export Login form
export default function Login() {
	// States
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);
	
	// To handle changes to our form inputs
	const handleEmail = (event) => {
		setEmail(event.target.value);
	};
	
	const handlePassword = (event) => {
		setPassword(event.target.value);
	};
	
	// To verify user input
	useEffect(() => {
		let isEmailNotEmpty = email !== "";
		let isPasswordNotEmpty = password !== "";
		
		if (isEmailNotEmpty && isPasswordNotEmpty) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password]);
	
	// Authenticate function that will log a simple login message and clear the email and password when user submits the form
	const submit = (event) => {
		event.preventDefault();

		setEmail("");
		setPassword("");
		
		alert("You are now logged in.");
	};
	
	return (
		<Container>
			<h3>Log In</h3>
			<Form id="login" onSubmit={submit}>
				<Form.Group controlId="formBasicEmail">
					<Form.Label>Email</Form.Label>
					<Form.Control 
						type="email" 
						value={email} 
						placeholder="Your email" 
						onChange={handleEmail} 
						required 
					/>
				</Form.Group>
				
				<Form.Group controlId="formBasicPassword">
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password" 
						value={password} 
						placeholder="Password" 
						onChange={handlePassword} 
						required 
					/>
				</Form.Group>
				
				<Button 
					variant="primary" 
					type="submit" 
					disabled={isDisabled}
				>
					Log In
				</Button>
			</Form>
		</Container>
	);
}