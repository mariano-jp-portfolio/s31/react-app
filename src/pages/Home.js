// HOME PAGE
// Base import
import React, { Fragment } from 'react';

// React-Bootstrap
import { Container } from 'react-bootstrap';

// Component import
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

// Export
export default function Home() {
	return (
		<Fragment>
			<Container>
				<Banner />
				<Highlights />
			</Container>
		</Fragment>
	);
}