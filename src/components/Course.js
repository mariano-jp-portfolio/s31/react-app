// Base import
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// React-bootstrap
import { Card, Button } from 'react-bootstrap';

// Export Course cards
// Course component is like a big object that stores properties passed unto it, this time it's the course object
export default function Course({course}) {
	// Destructuring course parameter
	const {name, description, price} = course;
	
	// State [ getters, setters ]
	// Array destructuring
	const [count, setCount] = useState(0); //count = 0
	const [seat, setSeat] = useState(10);
	const [isOpen, setIsOpen] = useState(true);

	// Hook
	// put an event listener in our button so that whenever it is clicked, it will invoke this hook
	function enroll() {
		setCount(count + 1);
		setSeat(seat - 1);
		// console.log("Enrollees: " + count); //output of 0 at first due to JS running codes synchronously
	}
	
	useEffect(() => {
		if (seat === 0) {
			setIsOpen(false);
		}
	}, [count, seat]);
	
	// If statement to determine if props contains values
	if (course) {
		return (
			<Card className="cardSpace">
				<Card.Body className="cardBG">
					<Card.Title><h2>{name}</h2></Card.Title>
					<Card.Text>
						<p className="subtitle">
							{description}
						</p>
						<p className="subtitle">
							&#8369;{price}
						</p>
					</Card.Text>
					<Card.Text>Enrollees: {count}</Card.Text>
					<Card.Text>Seats Available: {seat}</Card.Text>
					{
						isOpen ? <Button onClick={enroll} variant="success">Enroll</Button> : <Button onClick={enroll} variant="secondary" disabled>Fully Booked</Button>
					}
				</Card.Body>
			</Card>
		);
	} else {
		// props has no value, return ""
		return "";
	}
}

// Checks if the Course component is getting the correct prop structure/data type
Course.propTypes = {
	// course prop
	course: PropTypes.shape({
		// .shape() describes the structure of the object
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
};