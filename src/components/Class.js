// Base import
import React from 'react';

// Stateful Class (via named export)
export class Class extends React.Component {
	render() {
		return (
			<p>This is a class component exported via named export.</p>
		);
	}
}

export class ClassComp extends React.Component {
	render() {
		return (
			<p>This is also exported via named export.</p>
		);
	}
}