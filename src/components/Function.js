// Base import
import React from 'react';

// Stateless Functional Component
export default function Function() {
	return (
		<p>This is a stateless functional component.</p>
	);
}