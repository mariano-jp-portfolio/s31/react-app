// Base import
import React from 'react';

// React-Bootstrap
import { Jumbotron, Button, Row, Col } from 'react-bootstrap';

// Jumbotron function
export default function Banner() {
	return (
		<Row>
			<Col>
				<Jumbotron>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, anywhere</p>
					<p>
						<Button variant="warning">Enroll Now!</Button>
					</p>
				</Jumbotron>
			</Col>
		</Row>
	);
}