// Base import
import React from 'react';

// React-Bootstrap
import { Row, Col, Card } from 'react-bootstrap';

// Export Card function
export default function Highlights() {
	return (
		<Row>
			<Col>
				<Card className="cardSpace">
					<Card.Body className="cardBG">
						<Card.Title><h2>Learn from Home</h2></Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing, elit. Quae, quibusdam eligendi, facilis sunt, minima provident dolore expedita omnis est velit, voluptatum accusamus quis odio. Optio, quasi maxime earum dolor quia.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			
			<Col>
				<Card className="cardSpace">
					<Card.Body className="cardBG">
						<Card.Title><h2>Study Now Pay Later</h2></Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur, adipisicing elit. Explicabo suscipit, cupiditate animi assumenda. Mollitia minima excepturi, suscipit impedit commodi architecto ipsa alias magni aliquam, totam aspernatur eligendi rerum porro, voluptate!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			
			<Col>
				<Card className="cardSpace">
					<Card.Body className="cardBG">
						<Card.Title><h2>Be a Part of Our Community</h2></Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio quam tempore eaque recusandae sapiente quas corrupti facilis sequi ducimus perferendis enim veniam quo, fugit eligendi sint labore, quia quasi iure!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
}