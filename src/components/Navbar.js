// Base import
import React from 'react';

// React-Bootstrap
import {Navbar, Nav} from 'react-bootstrap';

// Navbar function
export default function NavBar() {
	return (
		<Navbar id="navbar" bg="dark" expand="lg">
			<Navbar.Brand href="#home">
				React.js
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link href="#home">
						Home
					</Nav.Link>
					<Nav.Link href="#link">
						Link
					</Nav.Link>
					<Nav.Link href="#login">
						Log In
					</Nav.Link>
					<Nav.Link href="#register">
						Register
					</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}