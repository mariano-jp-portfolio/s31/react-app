// Base import
import React from 'react';
import ReactDOM from 'react-dom';

// Index CSS
import './index.css';

// React-Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

// Components
import App from './App';

// Render
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);